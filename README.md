## Please see our GitHub repositories for information on commits, issues, and pull requests.
### [Organization](https://github.com/Trivial-Solution)
### [Backend](https://github.com/Trivial-Solution/Blindseer-Backend)
### [Frontend](https://github.com/Trivial-Solution/Blindseer-Frontend)
### [Raspberry Pi](https://github.com/Trivial-Solution/Blindseer-RaspberryPi)